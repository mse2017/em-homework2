clear variables;
% EM1- Hausaufgabe 1.2 - Dominik Brosch
F = [89 48];
a = 3;
b = 5;
c = sqrt(3^2+5^2);
K = [
    -1 0 0 1 0 0; %Knoten 1H | S1 S2 S3 A1 A2 B2
    0 1 0 0 -1 0; %Knoten 1V
    0 0 -a/c 0 0 0; %Knoten 2H
    0 -1 -b/c 0 0 0; %Knoten 2V
    1 0 b/c 0 0 0; %Knoten 3H
    0 0 a/c 0 0 -1 %Knoten 3V
    ];
L = [
    0;
    0;
    F(1);
    F(2);
    0;
    0
    ];
x = -K\L;
fprintf("Parameter:\n");
fprintf("\tFx:\t %d\n", F(1));
fprintf("\tFy:\t %d\n", F(2));
fprintf("\ta:\t %d\n", a);
fprintf("\tb:\t %d\n", b);
fprintf("Errechnete Kr�fte:\n");
fprintf("\tS1:\t %d\n", x(1));
fprintf("\tS2:\t %d\n", x(2));
fprintf("\tS3:\t %d\n", x(3));
fprintf("\tAh:\t %d\n", x(4));
fprintf("\tAv:\t %d\n", x(5));
fprintf("\tB:\t %d\n", x(6));

i = 1;
figure;
hold on;
adj = 0.063;
%PLOT F
Fnorm = sqrt(F(1)^2+F(2)^2);
quiver(0, -a, F(1)/Fnorm, F(2)/Fnorm, 'r');
%PLOT Ah
Ahnorm = sqrt(2*(x(4)^2));
quiver(0, 0, x(4)/Ahnorm, 0, 'g');
%PLOT Av
Avnorm = sqrt(2*(x(5)^2));
quiver(0, 0, 0,-(x(5)/Avnorm), 'g');
%PLOT Bv
Bvnorm = sqrt(2*(x(6)^2));
quiver(b, (x(6)/Bvnorm-adj), 0,-(x(6)/Bvnorm), 'g');

if (x(1)>0)
    plot([0 b], [0 0], 'b');
else
    plot([0 b], [0 0], 'r');
end
if (x(2)>0)
    plot([0 0], [0 -a], 'b');
else
    plot([0 0], [0 -a], 'r');
end
if (x(3)>0)
    plot([0 b], [-a 0], 'b');
else
    plot([0 b], [-a 0], 'r');
end
plot(0, 0, "*g");
plot(0, -a, "*g");
plot(b, 0, "*g");
axis([-1 8 -4 1]);
grid on;
xlabel("x");
ylabel("y");
title("Hausaufgabe 2.3");
legend("Kraft F1 (normiert)","Kraft Ah (normiert)","Kraft Av (normiert)","Kraft Bv (normiert)","Stab 1","Stab 2", "Stab 3", "Punkt 1", "Punkt 2", "Punkt 3");
dK = det(K);
rK = rank(K);
fprintf("Matrix Properties\n\tdet(K)=%d\n\trank(K)=%d\n",dK,rK);
